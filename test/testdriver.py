import cairo


STROKE_WIDTH = 1
WIDTH = 850
HEIGHT = 1100

surface = None
ctx = None

drawing = False

def init():
    global surface
    global ctx
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
    ctx = cairo.Context(surface)
    ctx.scale(1, 1)

    ctx.save()
    ctx.set_source_rgb(1, 1, 1)
    ctx.paint()
    ctx.restore()
        
    ctx.set_source_rgb(0, 0, 0)
    ctx.set_line_width(STROKE_WIDTH)

    ctx.move_to(0, 0)

def moveTo(x, y):
    global ctx
    if drawing:
        ctx.line_to(x, y)
    else:
        ctx.move_to(x, y)

def penDown():
    global drawing
    drawing = True

def penUp():
    global drawing
    global ctx
    if drawing:
        ctx.stroke()
    drawing = False

def currentPosition():
    global ctx
    return ctx.get_current_point()

def finish():
    global surface
    penUp()
    moveTo(0, 0)
    surface.write_to_png("test.png")

