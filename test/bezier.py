from servovg import path
from servovg import shape
from servovg import driver


def main():
    driver.init()

    points = (120, 160, 35, 200, 220, 260, 220, 40)
    shape.drawPolyLine(*points)

    path.moveTo(*points[:2])
    path.curveTo(*points[2:])

    driver.finish()


if __name__ == '__main__':
    main()
