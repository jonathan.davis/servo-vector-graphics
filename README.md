# Servo Vector Graphics

## Description

Servo Vector Graphics is the robot arm driver and API for our final project in ECE3161 Introduction to Robotics.
Accepts .svg files as input and draws them on a sheet of paper using a robotic arm.

