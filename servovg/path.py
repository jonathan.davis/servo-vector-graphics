from math import cos, sin, radians, sqrt
from servovg.driver import Driver
from servovg.util import angle, cubicBezier, quadraticBezier
import numpy as np

RESOLUTION = 1e3
driver = Driver()

##### Path segment commands #####

def moveTo(x, y):
    driver.penUp()
    driver.moveTo(x, y)

def moveToRelative(x, y):
    curX, curY = driver.currentPosition()   # Assumes commands are synchronous
    moveTo(curX + x, curY + y)

def closePath():
    raise NotImplementedError   # Will require keeping state for path

def lineTo(x, y):
    driver.penDown()
    driver.moveTo(x, y)

def lineToRelative(x, y):
    curX, curY = driver.currentPosition()
    lineTo(x + curX, y + curY)

def horizontalLineTo(x):
    curX, curY = driver.currentPosition()
    lineTo(x, curY)

def horizontalLineToRelative(x):
    curX, curY = driver.currentPosition()
    lineTo(curX + x, curY)

def verticalLineTo(y):
    curX, curY = driver.currentPosition()
    lineTo(curX, y)

def verticalLineToRelative(y):
    curX, curY = driver.currentPosition()
    lineTo(curX, curY + y)

def curveTo(x1, y1, x2, y2, x, y):
    x0, y0 = driver.currentPosition()
    for t in np.linspace(0, 1, RESOLUTION+1):
        nextX = cubicBezier(t, x0, x1, x2, x)
        nextY = cubicBezier(t, y0, y1, y2, y)
        lineTo(nextX, nextY)

def curveToRelative(x1, y1, x2, y2, x, y):
    curX, curY = driver.currentPosition()
    curveTo(curX + x1, curY + y1, curX + x2, curY + y2, curX + x, curY + y)

def smoothCurveTo(*points):
    raise NotImplementedError   # Requires keeping state of past Bezier commands

def quadraticCurveTo(x1, y1, x, y):
    x0, y0 = driver.currentPosition()
    for t in np.linspace(0, 1, RESOLUTION+1):
        nextX = quadraticBezier(t, x0, x1, x)
        nextY = quadraticBezier(t, y0, y1, y)
        lineTo(nextX, nextY)

def quadraticCurveToRelative(x1, y1, x, y):
    curX, curY = driver.currentPosition()
    quadraticCurveTo(curX + x1, curY + y1, curX + x, curY + y)

def smoothQuadraticCurveTo(*points):
    raise NotImplementedError   # Requires keeping state of past Bezier commands

def arc(cx, cy, rx, ry, rotation, theta1, theta2):
    x0, y0 = driver.currentPosition()
    cosR = cos(radians(rotation))
    sinR = sin(radians(rotation))

    for t in np.linspace(0, 1, RESOLUTION+1):
        theta = (1-t)*theta1 + t*theta2
        x, y = np.matmul(
                ((cosR, -sinR), (sinR, cosR)),
                (rx*cos(radians(theta)), ry*sin(radians(theta)))
                )
        x += cx
        y += cy
        lineTo(x, y)

def arcTo(rx, ry, rotation, largeArc, sweep, x, y):
    x0, y0 = driver.currentPosition()

    if x == x0 and y == y0:
        return
    if rx == 0 or ry == 0:
        lineTo(x, y)
        return
    rx = abs(rx)
    ry = abs(ry)

    cosR = cos(radians(rotation))
    sinR = sin(radians(rotation))
    x1p, y1p = np.matmul(
            ((cosR, sinR), (-sinR, cosR)),
            ((x0-x)/2, (y0-y)/2)
            )

    L = (x1p*x1p)/(rx*rx) + (y1p*y1p)/(ry*ry)
    if L > 1:
        rx *= sqrt(L)
        ry *= sqrt(L)

    center_coeff = sqrt(abs((rx*rx*ry*ry - rx*rx*y1p*y1p - ry*ry*x1p*x1p)/(rx*rx*y1p*y1p + ry*ry*x1p*x1p)))
    if largeArc == sweep:
        center_coeff *= -1
    cxp = center_coeff * rx*y1p/ry
    cyp = center_coeff * -ry*x1p/rx

    cx, cy = np.matmul(
            ((cosR, -sinR), (sinR, cosR)),
            (cxp, cyp)
            )
    cx += (x0+x)/2
    cy += (y0+y)/2

    theta1 = angle(1, 0, (x1p-cxp)/rx, (y1p-cyp)/ry)
    deltaTheta = angle((x1p-cxp)/rx, (y1p-cyp)/ry, (-x1p-cxp)/rx, (-y1p-cyp)/ry) % 360
    if deltaTheta > 0 and not sweep:
        deltaTheta -= 360
    elif deltaTheta < 0 and sweep:
        deltaTheta += 360

    arc(cx, cy, rx, ry, rotation, theta1, theta1 + deltaTheta)

def arcToRelative(rx, ry, rotation, largeArc, sweep, x, y):
    curX, curY = driver.currentPosition()
    arcTo(rx, ry, rotation, largeArc, sweep, curX + x, curY + y)

