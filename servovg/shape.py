from servovg import driver
from servovg import path


def drawLine(x1, y1, x2, y2):
    path.moveTo(x1, y1)
    path.lineTo(x2, y2)

def drawPolyLine(*points):
    assert(len(points) >= 2)        # Ensure there is at least one coordinate
    assert(len(points) % 2 == 0)    # Ensure that there are an even number of points

    x0, y0 = points[0], points[1]
    
    path.moveTo(x0, y0)
    for i in range(2, len(points), 2):
        x, y = points[i], points[i+1]
        path.lineTo(x, y)

def drawRect(x, y, width, height, rx, ry):
    isRounded = (rx > 0) and (ry > 0)

    path.moveTo(x + rx, y)
    path.horizontalLineTo(x + width - rx)
    if isRounded:
        path.arcTo(rx, ry, 0, False, True, x + width, y + ry)
    path.verticalLineTo(y + height - ry)
    if isRounded:
        path.arcTo(rx, ry, 0, False, True, x + width - rx, y + height)
    path.horizontalLineTo(x + rx)
    if isRounded:
        path.arcTo(rx, ry, 0, False, True, x, y + height - ry)
    path.verticalLineTo(y + ry)
    if isRounded:
        path.arcTo(rx, ry, 0, False, True, x + rx, y)

def drawCircle(x, y, r):
    drawEllipse(x, y, r, r)

def drawEllipse(x, y, rx, ry):
    path.moveTo(x + rx, y)
    path.arcTo(rx, ry, 0, False, True, x, y + ry)
    path.arcTo(rx, ry, 0, False, True, x - rx, y)
    path.arcTo(rx, ry, 0, False, True, x, y - ry)
    path.arcTo(rx, ry, 0, False, True, x + rx, y)

def drawPolygon(*points):
    assert(len(points) >= 2)        # Ensure there is at least one coordinate
    assert(len(points) % 2 == 0)    # Ensure that there are an even number of points

    x0, y0 = points[0], points[1]
    
    path.moveTo(x0, y0)
    for i in range(2, len(points), 2):
        x, y = points[i], points[i+1]
        path.lineTo(x, y)
    path.lineTo(x0, y0)

