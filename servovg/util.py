from math import degrees
from numpy import arccos, dot
from numpy.linalg import norm


def quadraticBezier(t, w0, w1, w2):
    t2 = t*t
    mt = 1-t
    mt2 = mt*mt
    return w0*mt2 + 2*w1*mt*t + w2*t2

def cubicBezier(t, w0, w1, w2, w3):
    t2 = t*t
    t3 = t2*t
    mt = 1-t
    mt2 = mt*mt
    mt3 = mt2*mt
    return w0*mt3 + 3*w1*mt2*t + 3*w2*mt*t2 + w3*t3

def angle(x1, y1, x2, y2):
    u = (x1, y1) / norm((x1, y1))
    v = (x2, y2) / norm((x2, y2))
    ang = arccos(dot(u, v))
    if x1*y2 - x2*y1 < 0:
        ang *= -1
    return degrees(ang)

