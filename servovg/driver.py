# driver.py 
# Driver to control hardware (i.e. move stepper motors and servo)

import RPi.GPIO as GPIO
import numpy as np
import time 
import math

MOTOR_STEP = 0.0314159    # 1.8 degress per step = 0.0314159 radians per step
MOTOR_STEP_SHOULDER_RATIO = 8    # 1 for whole steps, 2 for half steps, etc.
MOTOR_STEP_ELBOW_RATIO = 2
STEP_TIME = .008 #.01 # Time it takes for one step

class ShoulderMotor:
   # Shoulder controlled using Sparkfun Easydriver
   def __init__(self):
      # Pins
      shoulder_ms1 = 23
      shoulder_ms2 = 25
      shoulder_en = 24
      self.shoulder_step = 8
      self.shoulder_dir = 7
      
      GPIO.setmode(GPIO.BCM)     
      GPIO.setup(shoulder_ms1,GPIO.OUT)
      GPIO.setup(shoulder_ms2,GPIO.OUT)
      GPIO.output(shoulder_ms1, GPIO.HIGH)  # Microstepping in 1/2s is H,L, 1/8ths is H,H
      GPIO.output(shoulder_ms2, GPIO.HIGH)
      GPIO.setup(shoulder_en,GPIO.OUT)
      GPIO.output(shoulder_en, GPIO.LOW)  # Enable right away
      GPIO.setup(self.shoulder_step,GPIO.OUT)
      GPIO.setup(self.shoulder_dir,GPIO.OUT)
   
   # Steps the shoulder motor given desired direction of step
   # Accepts direction (+1 or -1)
   def step(self, direction=1):
      assert(direction ==-1 or direction == 1)  # Do at least a bit of error checking :P
      if direction==1:
         GPIO.output(self.shoulder_dir, GPIO.HIGH)
      else:
         GPIO.output(self.shoulder_dir, GPIO.LOW) 
      GPIO.output(self.shoulder_step, GPIO.HIGH)
      time.sleep(1E-5)
      GPIO.output(self.shoulder_step, GPIO.LOW)
      time.sleep(1E-5)


class ElbowMotor:
   # Elbow controlled using L298n motor driver
   def __init__(self):
      # Pins
      self.elbow_out1 = 2
      self.elbow_out2 = 3
      self.elbow_out3 = 4
      self.elbow_out4 = 17
      
      GPIO.setmode(GPIO.BCM)
      GPIO.setup(self.elbow_out1,GPIO.OUT)
      GPIO.setup(self.elbow_out2,GPIO.OUT)
      GPIO.setup(self.elbow_out3,GPIO.OUT)
      GPIO.setup(self.elbow_out4,GPIO.OUT)
      
      # Possible states for a half step
      self.STATES = ((1,0,0,1),(0,0,0,1),(0,1,0,1),(0,1,0,0),(0,1,1,0),(0,0,1,0),(1,0,1,0),(1,0,0,0))
      self.state = 0  # current state
      self.step() # Need to snap to current state
      
   # Steps the elbow motor given desired direction of step
   # Accepts direction (+1 or -1)
   def step(self, direction=1):
      assert(direction ==-1 or direction == 1)  # Do at least a bit of error checking :P
      self.state += direction  # step in the given
      if self.state == 8:
         self.state = 0
      elif self.state == -1:
         self.state = 7
      GPIO.output(self.elbow_out1, self.STATES[self.state][0])
      GPIO.output(self.elbow_out2, self.STATES[self.state][1])
      GPIO.output(self.elbow_out3, self.STATES[self.state][2])
      GPIO.output(self.elbow_out4, self.STATES[self.state][3])
      
class PenServo:
   def __init__(self):
      servo_pin = 18
      GPIO.setup(servo_pin, GPIO.OUT)
      self.servo = GPIO.PWM(servo_pin,50)
      self.servo.start(0)  # should start by not moving 0=stopped, 5%=full reverse, 10%=full forward
   
   def penUp(self):
      self.servo.ChangeDutyCycle(7.7)
      time.sleep(.08) 
      self.servo.ChangeDutyCycle(0)
      time.sleep(.5)

   def penDown(self):
      self.servo.ChangeDutyCycle(7.2)
      time.sleep(.3)
      self.servo.ChangeDutyCycle(7.3)
      time.sleep(.7)
      self.servo.ChangeDutyCycle(0)


class Driver:
   def __init__(self):
      self.theta1=0  # Current angles store the state of the arm
      self.theta2=0
      self.shoulder = ShoulderMotor()
      self.elbow = ElbowMotor()
      self.pen = PenServo()      
      self.tpage_0 = np.mat(np.eye(4))#((1, 0, 0, 6),(0, -1, 0, 5.5),(0, 0, -1, 0),(0,0,0,1))) # TODO make actual matrix np.mat(())      
   
   def penUp(self):
      self.pen.penUp()

   def penDown(self):
      self.pen.penDown()

   # Accepts desired x and y locations
   def moveTo(self, des_x, des_y):
      des_x = des_x/80+6
      des_y = 3-des_y/80
      curr_x,curr_y = self.currentPosition()
      print("%f %f %f %f "% (curr_x, curr_y, des_x, des_y))
      dx = des_x-curr_x
      dy = des_y-curr_y
      dist = math.sqrt((dx)**2+(dy)**2)
      num_segments = max(1,math.floor(dist*5))  # 5 segments per inch
      for i in range(1,int(num_segments+1)):
         print("moving to segment(x,y): %f, %f" % (curr_x+dx*i/num_segments, curr_y+dy*i/num_segments))
         self._moveToHelper(curr_x+dx*i/num_segments, curr_y+dy*i/num_segments)        
      time.sleep(1)
      
   def _moveToHelper(self, des_x, des_y):
      # Translate into frame 0 first, then inverse kinematics
      p0=np.transpose(self.tpage_0)*np.transpose(np.mat((des_x,des_y,0,1)))
      x0=p0[0,0]
      y0=p0[1,0]
      temp = (x0*x0+y0*y0-64-64)/(2*8*8)
      des_theta2=math.atan2(-math.sqrt(1-temp*temp),temp)
      des_theta1=math.atan2(y0,x0)-math.atan2(8*math.sin(des_theta2),8+8*math.cos(des_theta2))
      
      self.bresenham(des_theta1, des_theta2)
      
      #print("(des_x,des_y),(des_theta1, des_theta2): (%f,%f), (%f,%f)" % (des_x, des_y, des_theta1,des_theta2))
   
   # Returns current coordinates as tuple of (x,y) relative to paper
   def currentPosition(self):
      c=math.cos
      s=math.sin
      t1=self.theta1
      t2=self.theta2
      # Trasformation matrix of end effector (frame 3) w.r.t. base of robot (frame 0)
      t0_3 = np.mat(((c(t1+t2),-s(t1+t2), 0, 8*c(t1)+8*c(t1+t2)),
                     (s(t1+t2), c(t1+t2), 0, 8*s(t1)+8*s(t1+t2)),
                     (0,0,0,0),
                     (0,0,0,1)))
      ppage = self.tpage_0*t0_3*np.transpose(np.mat((0,0,0,1)))
      current_x = ppage[0,0]
      current_y = ppage[1,0]
      return current_x, current_y

   # Bresenham line algo applied to interpolating between motor steps
   def bresenham(self, des_theta1, des_theta2):
      # Get change in theta
      dtheta1 = des_theta1 - self.theta1
      dtheta2 = des_theta2 - self.theta2

      # Theta increments change based on direction
      theta1i = MOTOR_STEP / MOTOR_STEP_SHOULDER_RATIO
      theta2i = MOTOR_STEP / MOTOR_STEP_ELBOW_RATIO
      theta1_dir = theta2_dir = 1
      if dtheta1 < 0:
         theta1i *= -1
         theta1_dir = -1
         dtheta1 *= -1
      if dtheta2 < 0:
         theta2i *= -1
         theta2_dir = -1
         dtheta2 *= -1

      # Choose primary step (which joint moves more)
      if abs(dtheta2)*MOTOR_STEP_ELBOW_RATIO < abs(dtheta1)*MOTOR_STEP_SHOULDER_RATIO:
         D = 2*dtheta2*MOTOR_STEP_ELBOW_RATIO - dtheta1*MOTOR_STEP_SHOULDER_RATIO
         numSteps = round(abs(dtheta1 / theta1i))
         for step in range(numSteps):
            self.shoulder.step(theta1_dir)
            self.theta1 += theta1i
            if D > 0:
               self.elbow.step(theta2_dir)
               self.theta2 += theta2i
               D -= 2*dtheta1*MOTOR_STEP_SHOULDER_RATIO
            D += 2*dtheta2*MOTOR_STEP_ELBOW_RATIO
            time.sleep(STEP_TIME)
      else:
         D = 2*dtheta1*MOTOR_STEP_SHOULDER_RATIO - dtheta2*MOTOR_STEP_ELBOW_RATIO
         numSteps = round(abs(dtheta2 / theta2i))
         for step in range(numSteps):
            self.elbow.step(theta2_dir)
            self.theta2 += theta2i
            if D > 0:
               self.shoulder.step(theta1_dir)
               self.theta1 += theta1i
               D -= 2*dtheta2*MOTOR_STEP_ELBOW_RATIO
            D += 2*dtheta1*MOTOR_STEP_SHOULDER_RATIO
            time.sleep(STEP_TIME*2)
  
   def __del__(self):
      #GPIO.output(24, GPIO.HIGH)
      GPIO.cleanup()  # clean up GPIO upon program exit

#TODO TESTING ONLY  
#import sys
driver = Driver()
driver.pen.servo.ChangeDutyCycle(7.2)
time.sleep(.3)
driver.pen.servo.ChangeDutyCycle(7.3)
time.sleep(.7)
driver.pen.servo.ChangeDutyCycle(0)
driver.bresenham(0,1)
time.sleep(2)
driver.bresenham(0,-1)
time.sleep(2)
driver.pen.servo.ChangeDutyCycle(7.7)
time.sleep(.08)

#driver.penDown()
#driver.bresenham(0,math.pi/2)
#time.sleep(1)
#driver.bresenham(0,-math.pi/2)
#print(driver.currentPosition())
#driver._moveToHelper(8,0)
#time.sleep(1)
#driver.penDown()
#time.sleep(.2)
#print(driver.currentPosition())
#driver._moveToHelper(10,4)
#print(driver.currentPosition())
#time.sleep(1)
#driver.penUp()
#time.sleep(.2)
#driver.moveTo(16,0)
#time.sleep(1)
#driver.pen.pen_down()
#time.sleep(1)
#if len(sys.argv)<3:
#   print("not enough args")
   #print("Stepping shoulder 10 times")
   #for i in range(10):
   #   driver.shoulder.step()
#else:
#   print("Moving to (theta1,theta2)=(%f,%f)" % (float(sys.argv[1]),float(sys.argv[2])))
#   driver.bresenham(float(sys.argv[1]),float(sys.argv[2]))
   #print("Moving by steps for (theta1, theta2)=(%d,%d)" % (int(sys.argv[1]),int(sys.argv[2])))
   #for i in range(abs(int(sys.argv[1]))):
   #   if int(sys.argv[1])<0:
   #      driver.shoulder.step(-1)
   #   else:
   #      driver.shoulder.step(1)
   #   time.sleep(.3)
   #for i in range(abs(int(sys.argv[2]))):
   #   if int(sys.argv[2])<0:
   #      driver.elbow.step(-1)
   #   else:
   #      driver.elbow.step(1)
   #   time.sleep(.3)

